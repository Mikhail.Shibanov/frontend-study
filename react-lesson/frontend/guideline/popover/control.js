var popoverContainer = document.querySelectorAll('.popover');

popoverContainer.forEach(function (item) {
    var btn = item.querySelector('.popover-button');
    var toggle = item.querySelector('.popover-toggle');
    var trigger = item.dataset.trigger;

    if (trigger === 'click') {
        btn.onclick = function () {
            if (toggle.style.display !== 'none') {
                toggle.style.display = 'none'
            } else {
                toggle.style.display = 'inline-block'
            }

        };
    } else if (trigger === 'hover') {
        btn.onmouseover = function () {
            toggle.style.display = 'inline-block';
        };
        btn.onmouseout = function () {
            toggle.style.display = 'none';
        };
    }

})