var webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'dev';


const path = require('path')

module.exports = {
    entry: ['babel-polyfill', './frontend/entry.js'],
    output: {
        filename: 'nice-webpack.bundle.js',
        path: __dirname + '/public'
    },

    module: {
        rules: [
            {
                test: [/\.less?$/, /\.css$/],
                include: includeLocations(),
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {loader: 'less-loader'}
                ]
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                include: includeLocations(),
                use: [{
                    loader: 'url-loader'
                }]
            },
            {
                test: [/\.jsx?$/, /\.js$/],
                include: includeLocations(),
                use: [{
                    loader: 'babel-loader',
                    options: {
                        plugins: [
                            'transform-runtime'
                        ],
                        presets: [
                            'env',
                            'stage-0',
                            'react'
                        ]
                    },
                }]
            }

        ]
    },

    watch: NODE_ENV === 'dev',

    plugins: [
        new webpack.DefinePlugin({ NODE_ENV: JSON.stringify(NODE_ENV) }),
        new webpack.ProvidePlugin({
            React: 'react',
            ReactDOM: 'react-dom',
            NODE_ENV: NODE_ENV
        }),
    ],

    resolve: {
        modules: ['node_modules'],
        extensions: ['.js', '.jsx']
    },

    resolveLoader: {
        modules: ['node_modules'],
        extensions: ['.js', '.jsx']
    },






}

function includeLocations() {
    return [
        path.resolve(__dirname, './backend'),
        path.resolve(__dirname, './frontend')
    ]
}